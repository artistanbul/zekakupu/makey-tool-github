Makey Tool
==========

Turkcell ve MEB ortaklığında gerçekleştirilen Zeka Küpü projesi kapsamında 20 bin öğrenciye Maker ve Kodlama Kiti dağıtılarak kodlama ve robotik alanında kişisel gelişimleri sağlanması hedeflenmiştir.

Makey Tool aracı, öğrencilerin geliştirmiş oldukları projeleri görselleştirip [Turkcell Zeka Küpü Eğitim Portalı](https://zekakupulms.turkcell.com.tr)’nda paylaşabilmeleri için geliştirilmiştir.

Katkıcılar
----------

* Hakan Tatlı, @[nehiryeli](https://github.com/nehiryeli)
* Oğuzhan Kayar @[oguzhank77](https://github.com/oguzhank77)
* Onur Güzel @[onurguzel](https://github.com/onurguzel)
