File.prototype.convertToBase64 = function(callback){
    var reader = new FileReader();
    reader.onload = function(e) {
        callback(e.target.result)
    };
    reader.onerror = function(e) {
        callback(null);
    };
    reader.readAsDataURL(this);
};

function checkProjectName() {
    if ($('#inputProject').val().length > 1) {
        return true;
    } else {
        alert('Proje ismi giriniz');
        return false;
    }
}

function postServer() {
    if (!checkProjectName()) {
        return false;
    }

    $('.helper-btn').hide();
    html2canvas($("#circuit"), {
        onrendered: function(canvas) {
            var theCanvas = canvas;
            var dataURL= canvas.toDataURL();

            $('.uploadModal').modal('show');
            $.ajax({
                type: "POST",
                url: "here goes server URL",
                data: {
                    imgBase64: dataURL
                }
            }).done(function(o) {
                console.log('saved');
            });
        }
    });
    $('.helper-btn').show();
}


$(function () {
    var circuit = $('#circuit');

    $('html').keyup(function(e) {
        if(e.keyCode === 46) {
            $('.ui-selected').fadeOut(300, function(){
                $(this).remove();
            });
        }
    });
    circuit.click(function(e) {
        if (e.target !== this)
            return;
        $(".ui-selected").removeClass('ui-selected');

    });


    // Components accordion
    $('.collapse').on('shown.bs.collapse', function() {
        $(this).parent().find(".glyphicon-chevron-down").removeClass("glyphicon-chevron-down").addClass("glyphicon-minus");
    }).on('hidden.bs.collapse', function() {
        $(this).parent().find(".glyphicon-minus").removeClass("glyphicon-minus").addClass("glyphicon-chevron-down");
    });

    $("#components div").on( "click", function() {
        var elem =($(this).attr('data-name'));

        $('#items .'+elem ).clone().appendTo( "#circuit" );

        $(".draggable").click(function(event) {
            $(".ui-selected").removeClass('ui-selected');

            var selected = $(".ui-selected").each(function() {
                var el = $(this);
                el.data("offset", el.offset());
            });

            if(!$(this).hasClass("ui-selected")) {
                $(this).addClass("ui-selected");
            }
            var offset = $(this).offset();
        });

        $(".draggable").draggable({
            start: function(ev, ui) {
            },
            drag: function(event, ui) {
                var factor = (1 / zoomLevel) -1;

                ui.position.top += Math.round((ui.position.top - ui.originalPosition.top) * factor);
                ui.position.left += Math.round((ui.position.left- ui.originalPosition.left) * factor);

                var draggable = $(this).data("ui-draggable");
                $.each(draggable.snapElements, function(index, element) {
                    if(element.snapping) {

                        draggable._trigger("snapped", event, $.extend({}, ui, {
                            snapElement: $(element.item)
                        }));
                    }
                });
            },
            snap: true,
            snapMode: "outer",

            stop: function()
            {
                $.playSound('audio/snap');
            },
            //refreshPositions: true,
            snapped: function(event, ui) {
                $.playSound('audio/magnetic');
            }
        });
    });

    // Circuit zoom
    var zoomLevel = 1.0;
    $('.glyphicon-zoom-in').click(function() {
        if (zoomLevel <= 1.3){
            circuit.animate({ 'zoom': zoomLevel += .1 });
            var h = parseInt(circuit.css("height")) - parseInt(circuit.css("height")) * 10 / 100;
            circuit.css("height", h);
        }
    });

    $('.glyphicon-zoom-out').click(function() {
        if (zoomLevel >= 0.8){
            circuit.animate({ 'zoom': zoomLevel -= .1 });
            var h = parseInt(circuit.css("height")) + parseInt(circuit.css("height")) * 12 / 100;
            circuit.css("height", h);
        }
    });

    // Trash
    $("#trashbin").droppable({
        drop: function(event, ui) {
            $(ui.draggable).hide('slow');
        }
    });

    // Save
    $('.glyphicon-floppy-disk').parent().click(function() {
        if (!checkProjectName()) {
            return false;
        }

        $(".project-name").html($('#inputProject').val());
        $('.helper-btn').hide();
        html2canvas(circuit, {
            onrendered: function(canvas) {
                var dataURL= canvas.toDataURL();

                var name = $('#inputProject').val();
                download(dataURL, name, "image/png");
                $(".project-name").html("");

                $('.helper-btn').show();
            }
        });
    });

    $('.glyphicon-print').parent().click(function() {
        if (!checkProjectName()) {
            return false;
        }

        $(".project-name").html($('#inputProject').val());
        $('.helper-btn').hide();
        $('#circuit').addClass('whiteBg');
        html2canvas(circuit, {
            onrendered: function(canvas) {
                var dataURL= canvas.toDataURL();

                var $canvas = $('#canvas');
                $canvas.html("");
                $canvas.append('<img src="'+dataURL+'"</img>');

                $canvas.print();
                $canvas.html("");
                $(".project-name").html("");
                var name = $('#inputProject').val();
                $('.helper-btn').show();
                $('#circuit').removeClass('whiteBg');
            }
        });
    });

    $.extend({
        playSound: function() {
            return $(
                '<audio autoplay="autoplay" style="display:none;">'
                + '<source src="' + arguments[0] + '.mp3" />'
                + '<source src="' + arguments[0] + '.ogg" />'
                + '<embed src="' + arguments[0] + '.mp3" hidden="true" autostart="true" loop="false" class="playSound" />'
                + '</audio>'
            ).appendTo('body');
        }
    });

    document.getElementById('inputProject').value = window.location.search.substring(1).split("=")[1];
});
